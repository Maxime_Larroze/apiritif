<?php

use App\Http\Controllers\API\GameController;
use App\Http\Controllers\API\InfosParticipantController;
use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\PartyController;
use App\Http\Controllers\API\QuizzController;
use App\Http\Controllers\API\RoundController;
use App\Http\Controllers\API\RoundParticipationsController;
use App\Http\Controllers\API\TypeGamePointsController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\VotedGameController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/v1')->group(function () {
    Route::prefix('/public')->group(function () {
        Route::middleware(['guest','api'])->group(function () {
            Route::post('/login', [LoginController::class, 'login'])->name('api.public.login');
        });
    });

    Route::middleware(['api', 'auth:sanctum'])->group(function () {
        Route::prefix('/auth')->group(function () {
            Route::get('/logout', [LoginController::class, 'logout'])->name('api.auth.logout');

            // CRUD GAME
            Route::get('/games', [GameController::class, 'index'])->name('api.auth.games.index');
            Route::get('/games/{id}', [GameController::class, 'show'])->name('api.auth.games.show');
            Route::post('/games', [GameController::class, 'store'])->name('api.auth.games.store');
            Route::put('/games/{id}', [GameController::class, 'update'])->name('api.auth.games.update');
            Route::delete('/games/{id}', [GameController::class, 'destroy'])->name('api.auth.games.destroy');

            // CRUD INFO PARTICIPANT
            Route::get('/players', [InfosParticipantController::class, 'index'])->name('api.auth.players.index');
            Route::get('/players/{id}', [InfosParticipantController::class, 'show'])->name('api.auth.players.show');
            Route::post('/players', [InfosParticipantController::class, 'store'])->name('api.auth.players.store');
            Route::put('/players/{id}', [InfosParticipantController::class, 'update'])->name('api.auth.players.update');
            Route::delete('/players/{id}', [InfosParticipantController::class, 'destroy'])->name('api.auth.players.destroy');

            // CRUD PARTY
            Route::get('/parties', [PartyController::class, 'index'])->name('api.auth.parties.index');
            Route::get('/parties/{id}', [PartyController::class, 'show'])->name('api.auth.parties.show');
            Route::post('/parties', [PartyController::class, 'store'])->name('api.auth.parties.store');
            Route::put('/parties/{id}', [PartyController::class, 'update'])->name('api.auth.parties.update');
            Route::delete('/parties/{id}', [PartyController::class, 'destroy'])->name('api.auth.parties.destroy');

            // CRUD QUIZZ
            Route::get('/quizzs', [QuizzController::class, 'index'])->name('api.auth.quizzs.index');
            Route::get('/quizzs/{id}', [QuizzController::class, 'show'])->name('api.auth.quizzs.show');
            Route::post('/quizzs', [QuizzController::class, 'store'])->name('api.auth.quizzs.store');
            Route::put('/quizzs/{id}', [QuizzController::class, 'update'])->name('api.auth.quizzs.update');
            Route::delete('/quizzs/{id}', [QuizzController::class, 'destroy'])->name('api.auth.quizzs.destroy');

            // CRUD ROUND
            Route::get('/rounds', [RoundController::class, 'index'])->name('api.auth.rounds.index');
            Route::get('/rounds/{id}', [RoundController::class, 'show'])->name('api.auth.rounds.show');
            Route::post('/rounds', [RoundController::class, 'store'])->name('api.auth.rounds.store');
            Route::put('/rounds/{id}', [RoundController::class, 'update'])->name('api.auth.rounds.update');
            Route::delete('/rounds/{id}', [RoundController::class, 'destroy'])->name('api.auth.rounds.destroy');

            // CRUD ROUND PARTICIPATIONS
            Route::get('/round-players', [RoundParticipationsController::class, 'index'])->name('api.auth.round-players.index');
            Route::get('/round-players/{id}', [RoundParticipationsController::class, 'show'])->name('api.auth.round-players.show');
            Route::post('/round-players', [RoundParticipationsController::class, 'store'])->name('api.auth.round-players.store');
            Route::put('/round-players/{id}', [RoundParticipationsController::class, 'update'])->name('api.auth.round-players.update');
            Route::delete('/round-players/{id}', [RoundParticipationsController::class, 'destroy'])->name('api.auth.round-players.destroy');

            // CRUD TYPE GAME POINTS
            Route::get('/types', [TypeGamePointsController::class, 'index'])->name('api.auth.types.index');
            Route::get('/types/{id}', [TypeGamePointsController::class, 'show'])->name('api.auth.types.show');
            Route::post('/types', [TypeGamePointsController::class, 'store'])->name('api.auth.types.store');
            Route::put('/types/{id}', [TypeGamePointsController::class, 'update'])->name('api.auth.types.update');
            Route::delete('/types/{id}', [TypeGamePointsController::class, 'destroy'])->name('api.auth.types.destroy');

            // CRUD USER
            Route::get('/users', [UserController::class, 'index'])->name('api.auth.users.index');
            Route::get('/users/{id}', [UserController::class, 'show'])->name('api.auth.users.show');
            Route::post('/users', [UserController::class, 'store'])->name('api.auth.users.store');
            Route::put('/users/{id}', [UserController::class, 'update'])->name('api.auth.users.update');
            Route::delete('/users/{id}', [UserController::class, 'destroy'])->name('api.auth.users.destroy');

            // CRUD VOTED GAME
            Route::get('/voted-games', [VotedGameController::class, 'index'])->name('api.auth.voted-games.index');
            Route::get('/voted-games/{id}', [VotedGameController::class, 'show'])->name('api.auth.voted-games.show');
            Route::post('/voted-games', [VotedGameController::class, 'store'])->name('api.auth.voted-games.store');
            Route::put('/voted-games/{id}', [VotedGameController::class, 'update'])->name('api.auth.voted-games.update');
            Route::delete('/voted-games/{id}', [VotedGameController::class, 'destroy'])->name('api.auth.voted-games.destroy');

        });
    });
});
