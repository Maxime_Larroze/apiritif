<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WEB\PartyController;
use App\Http\Controllers\WEB\FO\HomeController;
use App\Http\Controllers\WEB\FO\ViewController;
use App\Http\Controllers\WEB\FO\LoginController;
use App\Http\Controllers\WEB\BO\QuestionController;
use App\Http\Controllers\WEB\BO\BackOfficeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// UTILISATION WEB COTE FO
Route::middleware('guest')->group(function () {
    Route::get('/connexion', [LoginController::class, 'show'])->name('web.public.show');
    Route::post('/connexion', [LoginController::class, 'login'])->name('web.public.login');
    Route::post('/register', [LoginController::class, 'register'])->name('web.public.register');
    Route::get('/', [HomeController::class, 'index'])->name('web.public.index');
});

Route::middleware('auth')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('web.public.index');
    Route::prefix('/auth')->group(function () {
        Route::get('/home', [ViewController::class, 'showHome'])->name('web.auth.show.home');
        Route::get('/logout', [LoginController::class, 'logout'])->name('web.auth.logout');
        Route::get('/creation_partie', [PartyController::class, 'index'])->name('web.auth.party.creation');

        //-----------------------BO
        Route::get('/back-office', [BackOfficeController::class, 'index'])->name('web.auth.back.office');
        Route::get('/back-office/questions', [QuestionController::class, 'index'])->name('web.auth.back.office.question');
    });
});