<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('round_participations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('round_id');
            $table->unsignedBigInteger('player_id');
            $table->unsignedBigInteger('response_id'); //Où se trouve l'ID ?
            $table->timestamp('win')->nullable();
            $table->timestamps();
            $table->foreign('round_id')->references('id')->on('rounds');
            $table->foreign('player_id')->references('id')->on('users');
            $table->foreign('response_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('round_participations');
    }
};
