<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rounds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('game_id');
            $table->unsignedBigInteger('main_player_id');//servira si on ajoute les quizzs collectifs
            $table->unsignedBigInteger('party_id');
            $table->timestamps();
            $table->foreign('party_id')->references('id')->on('parties');
            $table->foreign('game_id')->references('id')->on('games');
            $table->foreign('main_player_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rounds');
    }
};
