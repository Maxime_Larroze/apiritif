<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voted_games', function (Blueprint $table) {
            $table->id();
            $table->string('instruction');
            $table->enum('type', ['Action', 'Verité', 'Mime', 'Duel']);
            $table->string('answer_1');
            $table->string('answer_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voted_games');
    }
};
