<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos_participants', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('party_id');
            $table->unsignedBigInteger('player_id');
            $table->timestamp('play_quizz')->nullable();
            $table->timestamp('play_action')->nullable();
            $table->timestamp('play_verite')->nullable();
            $table->timestamp('play_mime')->nullable();
            $table->timestamp('play_duel')->nullable();
            $table->integer('points')->default(0);
            $table->timestamps();
            $table->foreign('player_id')->references('id')->on('users');
            $table->foreign('party_id')->references('id')->on('parties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos_participants');
    }
};
