@include('template.BO.header')
@include('template.BO.navigation')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    h1{
    font-size: 50px;
    color: #26252a;
    text-transform: uppercase;
    font-weight: 300;
    text-align: center;
    margin-bottom: 15px;
  }
  table{
    width:100%;
    table-layout: fixed;
  }
  .tbl-header{
    background-color: #8fd398;
   }
  .tbl-content{
    height:300px;
    overflow-x:auto;
    margin-top: 0px;
    border: 1px solid #26252a;
  }
  th{
    padding: 20px 15px;
    text-align: left;
    font-weight: 500;
    font-size: 12px;
    color: #26252a;
    text-transform: uppercase;
  }
  td{
    padding: 15px;
    text-align: left;
    vertical-align:middle;
    font-weight: 300;
    font-size: 15px;
    color: #26252a;
    border-bottom: solid 1px #26252a;
  }
  
  
  /* demo styles */

  section{
    margin: 50px;
  }
  
  
 
  /* for custom scrollbar for webkit browser*/
  
  ::-webkit-scrollbar {
      width: 6px;
  } 
  ::-webkit-scrollbar-track {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
  } 
  ::-webkit-scrollbar-thumb {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
  }
</style>
<div class="container">
    <div class="row mt-5">
        <div class="col-12" id="questionsList">
            <div class="tbl-header">
              <table cellpadding="0" cellspacing="0" border="0">
                <thead>
                  <tr>
                    <th>Question ID</th>
                    <th>Question</th>
                    <th>Action</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div class="tbl-content">
              <table cellpadding="0" cellspacing="0" border="0">
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Qu'est ce qu'un triangle aux trois côtés égaux ?</td>
                    <td>
                        <a href=""><i class="fa-solid fa-pen text-dark mr-2"></i></a>
                        <a href=""><i class="fa-solid fa-trash text-dark"></i></a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
    </div>
        <div class="col-12 mt-5 bg-dark text-light p-5">
            <form>
                <div class="form-group">
                  <label for="questionCreate">Question</label>
                  <input type="text" class="form-control" id="questionCreate" aria-describedby="questionCreate" placeholder="Entrez une question">
                </div>
                <div class="row">
                    <div class="col">
                        <label for="answerA">Réponse A</label>
                        <input type="text" class="form-control" placeholder="First name" id="answerA" name="answerA">
                    </div>
                    <div class="col">
                        <label for="answerB">Réponse B</label>
                        <input type="text" class="form-control" placeholder="Last name" id="answerB" name="answerB">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col">
                        <label for="answerC">Réponse C</label>
                      <input type="text" class="form-control" placeholder="First name" id="answerC" name="answerC">
                    </div>
                    <div class="col">
                        <label for="answerD">Réponse D</label>
                      <input type="text" class="form-control" placeholder="Last name" id="answerD" name="answerD">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mt-4">Enregistrer</button>
            </form>
        </div>
    </div>
</div>


@include('template.footer')