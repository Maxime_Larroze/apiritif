@include('template.header')
    <div class="container">
        <div class="row" id="connexionModal">
            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4 mt-5"></div>
            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4 mt-5 card  connexionModal text-light">
                <div class="card-body p-md-5 mx-md-4">
                    <div class="text-center">
                        <img src="{{asset('images/FO/logo.png')}}" style="width: 185px;" alt="logo">
                        <img src="{{asset('images/FO/logoFont.png')}}" style="width: 185px;" alt="logo">

                        @error('error')
                            <div class="alert alert-danger alert-outline alert-dismissible mt-3" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                <div class="alert-icon"><i class="fas fa-exclamation-triangle"></i></div>
                                <div class="alert-message text-center">{{$message}}</div>
                            </div>
                        @enderror
                        @error('success')
                            <div class="alert alert-success alert-outline alert-dismissible" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                <div class="alert-icon"><i class="fas fa-check"></i></div>
                                <div class="alert-message text-center">{{$message}}</div>
                            </div>
                        @enderror
                        <form class="mt-3" method="post" action="{{route('web.public.login')}}">
                            @csrf
                            <div class="form-group">
                                <input type="email" class="form-control" id="username" name="username" placeholder="Votre email" value="{{old('username')}}">
                                @error('username')
                                    <div class="alert">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Votre mot de passe">
                                @error('password')
                                    <div class="alert">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group login-group-checkbox">
                                <input class="form-check-input" type="checkbox" id="remember"  name="remember">
                                <label class="form-check-label" for="remember">Rester connecté</label>
                            </div>

                            <div class="text-center pt-1 mb-5 pb-1 d-grid">
                                <button class="btn btn-block" type="submit">Se connecter</button>

                                <p class="createUser mt-3" id="">Pas de compte jeune mousse ? Crées un compte</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4 mt-5"></div>
        </div>


        <div class="row" id="registerModal">
            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4 mt-5"></div>
            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4 mt-5 card  connexionModal text-light">
                <div class="card-body p-md-5 mx-md-4">
                    <div class="text-center">
                        <img src="{{asset('images/FO/logo.png')}}" style="width: 185px;" alt="logo">
                        <img src="{{asset('images/FO/logoFont.png')}}" style="width: 185px;" alt="logo">
                        @error('error')
                            <div class="alert alert-danger alert-outline alert-dismissible mt-3" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                <div class="alert-icon"><i class="fas fa-exclamation-triangle"></i></div>
                                <div class="alert-message text-center">{{$message}}</div>
                            </div>
                        @enderror
                        @error('success')
                            <div class="alert alert-success alert-outline alert-dismissible" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                <div class="alert-icon"><i class="fas fa-check"></i></div>
                                <div class="alert-message text-center">{{$message}}</div>
                            </div>
                        @enderror
                        <form class="mt-3" method="post" action="{{route('web.public.register')}}">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" id="text" name="pseudo" placeholder="Votre pseudo" value="{{old('pseudo')}}">
                                @error('pseudo')
                                    <div class="alert">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Votre email" value="{{old('email')}}">
                                @error('email')
                                    <div class="alert">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Votre mot de passe" value="{{old('password')}}">
                                @error('password')
                                    <div class="alert">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" id="verificationPassword" name="verificationPassword" placeholder="Vérification du mot de passe" value="{{old('verificationPassword')}}">
                                @error('verificationPassword')
                                    <div class="alert">{{ $message }}</div>
                                @enderror
                            </div>
                            
                            {{-- <div class="form-group login-group-checkbox">
                                <input class="form-check-input" type="checkbox" id="remember"  name="remember">
                                <label class="form-check-label" for="remember">Rester connecté</label>
                            </div> --}}
                            
                            <div class="text-center pt-1 pb-1 d-grid">
                                <button class="btn btn-block" type="submit">Se connecter</button>
                                <p class="createUser mt-3" id="">Déjà un compte ? Connecte toi matelot !</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4 mt-5"></div>
        </div>
    </div>
</body>
</html>
@include('template.footer')
<script>
    
    $("#registerModal").hide();

    $(".createUser").on("click", function(){
        if ($("#connexionModal").is(":visible")) {
            $("#connexionModal").hide();
            $("#registerModal").show();
        } else if($("#registerModal").is(":visible")) {
            $("#connexionModal").show();
            $("#registerModal").hide();
        }
        
    })
</script>
