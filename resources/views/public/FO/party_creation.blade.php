@include('template.header')

<div class="container mt-5" id="partyForm">
    <div class="row mt-5">
        <div class="col-12 mt-5">
            <div class="box-shadow"></div>
            <h1 class="mb-5 text-center" id="partyTitle">Créer une partie !</h1>
        </div>
        <div class="col-6 mx-auto text-center mt-5 pe-5">
            <form action="" method="post">
                <div class="row">
                    <input class="text-center" type="text" name="nameParty" id="nameParty" placeholder="Nom de la partie">
                    <input class="text-center mt-3" type="number" min="1" name="playerNumber" id="playerNumber" placeholder="Nombre de matelots">
                    <button class="btn mt-3" type="submit" id="submitPartyForm">Hissez haut !</button>
                </div>
            </form>
        </div>
        
        <div class="col-6 mx-auto mt-5" id="playerSection">
            <h2 class="text-center">Joueurs de la partie</h2>
            <ul class="playerList">
                <li>
                    <img id="playerIcon" class="me-2" src="{{ asset('images/FO/coin.png') }}" alt="">
                    coucou
                </li>
            </ul>
        </div>
    </div>
</div>

@include('template.footer')