<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Apiritif | Hackenathon-System">
	<meta name="author" content="">
	<meta name="keywords" content="Apiritif">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <meta http-equiv="X-Content-Type-Options" content="nosniff">

	<title>API-Ritif - Portail</title>
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="shortcut icon" href="{{asset('images/logo/logo.png')}}" />
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/app_css.css')}}">
    <script src="{{mix('/js/app.js')}}"></script>

</head>
<body class="bg-img background-green">
	<nav class="navbar navbar-expand-lg justify-content-end">
		@if (auth()->check() == false)
			<a class="navbar-brand" href="{{route('web.public.show')}}"><img class="iconConnexion" src="{{asset('images/FO/logo.png')}}" alt="Connexion"></a>
		@elseif(auth()->check() == true)
			<a class="navbar-brand" href="{{route('web.auth.logout')}}"><img class="iconConnexion" src="{{asset('images/FO/skull.png')}}" alt="Connexion"></a>
		@endif
	</nav>
