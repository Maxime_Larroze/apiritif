<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use App\Models\RoundParticipations;
use App\Http\Requests\StoreRoundParticipationsRequest;
use App\Http\Requests\UpdateRoundParticipationsRequest;

class RoundParticipationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRoundParticipationsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoundParticipationsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RoundParticipations  $roundParticipations
     * @return \Illuminate\Http\Response
     */
    public function show(RoundParticipations $roundParticipations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RoundParticipations  $roundParticipations
     * @return \Illuminate\Http\Response
     */
    public function edit(RoundParticipations $roundParticipations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRoundParticipationsRequest  $request
     * @param  \App\Models\RoundParticipations  $roundParticipations
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoundParticipationsRequest $request, RoundParticipations $roundParticipations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RoundParticipations  $roundParticipations
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoundParticipations $roundParticipations)
    {
        //
    }
}
