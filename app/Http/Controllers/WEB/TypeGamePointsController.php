<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use App\Models\TypeGamePoints;
use App\Http\Requests\StoreTypeGamePointsRequest;
use App\Http\Requests\UpdateTypeGamePointsRequest;

class TypeGamePointsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTypeGamePointsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTypeGamePointsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeGamePoints  $typeGamePoints
     * @return \Illuminate\Http\Response
     */
    public function show(TypeGamePoints $typeGamePoints)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeGamePoints  $typeGamePoints
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeGamePoints $typeGamePoints)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTypeGamePointsRequest  $request
     * @param  \App\Models\TypeGamePoints  $typeGamePoints
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTypeGamePointsRequest $request, TypeGamePoints $typeGamePoints)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeGamePoints  $typeGamePoints
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeGamePoints $typeGamePoints)
    {
        //
    }
}
