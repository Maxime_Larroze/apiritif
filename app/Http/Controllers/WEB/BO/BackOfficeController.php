<?php

namespace App\Http\Controllers\WEB\BO;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackOfficeController extends Controller
{
    public function index(){
        return view('public/BO/index');
    }
}
