<?php

namespace App\Http\Controllers\WEB\BO;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    public function index(){
        $this->setToken(Auth::user()->api_token);
        $response = $this->http()->get($this->getURI().'auth/quizzs');
        // dd($response);
        if(empty($response->json()['error']))
        {
            $this->statusCode($response);
            // return view('auth.FO.index', ['user' => $response->json()['user']]);
        }


        return view('public/BO/question');
    }

    public function addQuestion(Request $request)
    {
        dd($request->all());
    }
}
