<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use App\Models\VotedGame;
use App\Http\Requests\StoreVotedGameRequest;
use App\Http\Requests\UpdateVotedGameRequest;

class VotedGameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreVotedGameRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVotedGameRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VotedGame  $votedGame
     * @return \Illuminate\Http\Response
     */
    public function show(VotedGame $votedGame)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VotedGame  $votedGame
     * @return \Illuminate\Http\Response
     */
    public function edit(VotedGame $votedGame)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateVotedGameRequest  $request
     * @param  \App\Models\VotedGame  $votedGame
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVotedGameRequest $request, VotedGame $votedGame)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VotedGame  $votedGame
     * @return \Illuminate\Http\Response
     */
    public function destroy(VotedGame $votedGame)
    {
        //
    }
}
