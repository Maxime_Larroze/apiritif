<?php

namespace App\Http\Controllers\WEB\FO;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ViewController extends Controller
{
    public function showHome()
    {
        $this->setToken(Auth::user()->api_token);
        $response = $this->http()->get($this->getURI().'auth/users/'.Auth::user()->id);
        if(empty($response->json()['error']))
        {
            $this->statusCode($response);
            // return view('public.FO.index', ['user' => $response->json()['user']]);
        }
        return view('public.FO.index');
    }
}
