<?php

namespace App\Http\Controllers\WEB\FO;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class LoginController extends Controller
{


    public function show()
    {
        return view('public/FO/index');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);
        if(empty($request->remember)) {$request->remember = false;}
        $response = Http::post($this->uri.'public/login', ['username'=>$request->username, 'password'=>$request->password, 'remember'=>$request->remember??true]);
        if(empty($response->json()['error']))
        {
            $this->statusCode($response);
            Auth::loginUsingId($response->json()['user']['id']);
            return redirect()->route('web.auth.show.home');
        }
        return redirect()->back();
    }


    public function register(Request $request)
    {
        /* $request retourne:
            "pseudo" => ""
            "mail" => ""
            "password" => ""
            "verficationPassword" => ""
        */

        $validator = Validator::make($request->all(), [
                "pseudo" => "required",
                "email" => "required|email|unique:users",
                "password" => "required",
                "verificationPassword" => "required|same:password"
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->errors())
                ->withInput($request->input());
        }

        // dd("coucou");
        // $response = $this->http()->METHODE HTTP($this->getURI().'URI DE DESTINATION' ['DATA SI BESOINS']);
        $response = $this->http()->post($this->getURI().'/users', [$request]);
        
        // $user = User::create([
        //     'pseudo' => request("username"),
        //     'email' => request("mail"),
        //     'password' => request("password"),
        // ]);

        // $user->save();

        return redirect()
        ->back();


        



        // dd($request->all());
    }

    public function logout()
    {
        $response = Http::get($this->uri.'auth/logout');
        $this->statusCode($response);
        Auth::logout();
        return redirect()->route('web.public.show');
    }
}
