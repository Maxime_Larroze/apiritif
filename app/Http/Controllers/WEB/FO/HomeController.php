<?php

namespace App\Http\Controllers\WEB\FO;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        if(Auth::user())
        {
            $response = $this->http()->get($this->getURI().'auth/users/'.Auth::user()->id);
        }
        // dd($response);
        return view('public/FO/home');
    }
}
