<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use App\Models\InfosParticipant;
use App\Http\Requests\StoreInfosParticipantRequest;
use App\Http\Requests\UpdateInfosParticipantRequest;

class InfosParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInfosParticipantRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInfosParticipantRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InfosParticipant  $infosParticipant
     * @return \Illuminate\Http\Response
     */
    public function show(InfosParticipant $infosParticipant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InfosParticipant  $infosParticipant
     * @return \Illuminate\Http\Response
     */
    public function edit(InfosParticipant $infosParticipant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInfosParticipantRequest  $request
     * @param  \App\Models\InfosParticipant  $infosParticipant
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInfosParticipantRequest $request, InfosParticipant $infosParticipant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InfosParticipant  $infosParticipant
     * @return \Illuminate\Http\Response
     */
    public function destroy(InfosParticipant $infosParticipant)
    {
        //
    }
}
