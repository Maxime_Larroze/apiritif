<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\VotedGame;
use App\Http\Requests\StoreVotedGameRequest;
use App\Http\Requests\UpdateVotedGameRequest;
use Illuminate\Http\Request;

class VotedGameController extends Controller
{
     /**
      *  @OA\Get(
      *      path="/v1/auth/voted-games",
      *      tags={"Voted Game"},
      *      summary="Get list of voted games",
      *      description="Returns all voted games",
      *      security={{ "apiAuth": {} }},
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/VotedGame")
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\MediaType(
      *            mediaType="application/json",
      *          )
      *      ),
      *      @OA\Response(
      *        response=400,
      *        description="Bad Request"
      *      ),
      *      @OA\Response(
      *        response=404,
      *        description="Not found"
      *      ),
      *  )
      */
    public function index()
    {
      $votedGames = VotedGame::all();
      return response()->json($votedGames, 200);
    }


    /**
     * @OA\Post(
     *      path="/v1/auth/voted-games",
     *      tags={"Voted Game"},
     *      summary="Store new voted game",
     *      description="Returns voted game data",
     *      security={{ "apiAuth": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="instruction", format="string", default="instruction"),
     *              @OA\Property(property="answer_1", format="string", default="answer_1"),
     *              @OA\Property(property="answer_2", format="string", default="answer_2"),
     *              @OA\Property(property="type", format="string", default="Action"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/VotedGame")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'instruction' => ['required', 'string'],
        'answer_1' => ['required', 'string'],
        'answer_2' => ['required', 'string'],
        'type' => ['required', 'in:Action,Verité,Mime,Duel'],
      ]);

      try {
        $votedGame = VotedGame::create([
          'instruction' => $request->instruction,
          'answer_1' => $request->answer_1,
          'answer_2' => $request->answer_2,
          'type' => $request->type,
        ]);
        return response()->json($votedGame, 201);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Unauthorized'], 403);
    }


     /**
      *  @OA\Get(
      *      path="/v1/auth/voted-games/{id}",
      *      tags={"Voted Game"},
      *      summary="Get one Voted Game",
      *      description="Returns one voted game",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Voted game id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/VotedGame")
      *      ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      )
      *  )
      */
    public function show(VotedGame $id)
    {
        $votedGame = VotedGame::find($id);
        
        try {
            return response()->json($votedGame, 200);
        }
        catch (\Throwable $th) {
            return response()->json(['error'=>'Unauthorized'], 403);
        }
        return response()->json(['error'=>'Voted Game Not Found'], 404);
    }


     /**
      * @OA\Put(
      *      path="/v1/auth/voted-games/{id}",
      *      tags={"Voted Game"},
      *      summary="Update existing voted game",
      *      description="Returns updated voted game data",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Voted game id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\RequestBody(
      *          required=true,
      *          @OA\JsonContent(
      *              @OA\Property(property="instruction", format="string", default="instruction"),
      *              @OA\Property(property="answer_1", format="string", default="answer_1"),
      *              @OA\Property(property="answer_2", format="string", default="answer_2"),
      *              @OA\Property(property="type", format="string", default="Action"),
      *          )
      *      ),
      *      @OA\Response(
      *          response=202,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/VotedGame")
      *       ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found",
      *          @OA\JsonContent()
      *      )
      * )
      */
    public function update(VotedGame $id, Request $request)
    {
        $this->validate($request, [
            'instruction' => 'nullable, string',
            'answer_1' => 'nullable, string',
            'answer_2' => 'nullable, string',
            'type' => 'nullable, in:Action,Verité,Mime,Duel',
        ]);

        $votedGame = VotedGame::find($id);

        try {
            $request->instruction ? $votedGame->instruction = $request->instruction : false;
            $request->answer_1 ? $votedGame->answer_1 = $request->answer_1 : false;
            $request->answer_2 ? $votedGame->answer_2 = $request->answer_2 : false;
            $request->type ? $votedGame->type = $request->type : false;
            $votedGame->save();

            return response()->json($votedGame, 200);
        }
        catch (\Throwable $th) {
            return response()->json(['error'=>'Unauthorized'], 403);
        }
        return response()->json(['error'=>'Voted Game Not Found'], 404);
    }


     /**
      * @OA\Delete(
      *      path="/v1/auth/voted-games/{id}",
      *      tags={"Voted Game"},
      *      summary="Delete a voted game by id",
      *      description="Deletes a record and returns no content",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Voted game id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=204,
      *          description="Successful operation",
      *          @OA\JsonContent()
      *       ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden"
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found"
      *      )
      * )
      */
    public function destroy(VotedGame $id)
    {
      $votedGame = VotedGame::find($id);

      try {
        $votedGame->delete();
        return response()->json(['success'=>'Voted game deleted with success'], 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Voted Game Not Found'], 404);
    }
}
