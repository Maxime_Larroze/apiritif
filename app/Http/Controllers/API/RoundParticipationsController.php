<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\RoundParticipations;
use App\Http\Requests\StoreRoundParticipationsRequest;
use App\Http\Requests\UpdateRoundParticipationsRequest;
use Illuminate\Http\Request;

class RoundParticipationsController extends Controller
{
     /**
      *  @OA\Get(
      *      path="/v1/auth/round-players",
      *      tags={"Round Players"},
      *      summary="Get list of Round Players",
      *      description="Returns all round players",
      *      security={{ "apiAuth": {} }},
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/RoundParticipations")
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\MediaType(
      *            mediaType="application/json",
      *          )
      *      ),
      *      @OA\Response(
      *        response=400,
      *        description="Bad Request"
      *      ),
      *      @OA\Response(
      *        response=404,
      *        description="Not found"
      *      ),
      *  )
      */
    public function index()
    {
      $roundsParticipations = RoundParticipations::all();
      return response()->json($roundsParticipations, 200);
    }


    /**
     * @OA\Post(
     *      path="/v1/auth/round-players",
     *      tags={"Round Players"},
     *      summary="Store new round players",
     *      description="Returns round players data",
     *      security={{ "apiAuth": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="round_id", format="integer", default="1"),
     *              @OA\Property(property="player_id", format="integer", default="1"),
     *              @OA\Property(property="response_id", format="integer", default="1"),
     *              @OA\Property(property="win", format="boolean", default="timestamp?"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/RoundParticipations")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'round_id' => ['required', 'integer'],
        'player_id' => ['required', 'integer'],
        'response_id' => ['required', 'integer'],
        'win' => 'required',
      ]);

      try {
        $roundParticipations = RoundParticipations::create([
          'round_id' => $request->round_id,
          'player_id' => $request->player_id,
          'response_id' => $request->response_id,
          'win' => $request->win,
        ]);
        return response()->json($roundParticipations, 201);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Unauthorized'], 403);
    }


     /**
      *  @OA\Get(
      *      path="/v1/auth/round-players/{id}",
      *      tags={"Round Players"},
      *      summary="Get one Round Players",
      *      description="Returns one round players",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Round players id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/RoundParticipations")
      *      ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      )
      *  )
      */
    public function show(RoundParticipations $id)
    {
        $roundParticipation = RoundParticipations::find($id);
      try {
        return response()->json($roundParticipation, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=> 'Round Players Not Found'], 404);
    }


     /**
      * @OA\Put(
      *      path="/v1/auth/round-players/{id}",
      *      tags={"Round Players"},
      *      summary="Update existing round players",
      *      description="Returns updated round players data",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Round players id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\RequestBody(
      *          required=true,
      *          @OA\JsonContent(
      *              @OA\Property(property="round_id", format="integer", default=""),
      *              @OA\Property(property="player_id", format="integer", default=""),
      *              @OA\Property(property="response_id", format="integer", default=""),
      *              @OA\Property(property="win", format="boolean", default="timestamp?"),
      *          )
      *      ),
      *      @OA\Response(
      *          response=202,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/RoundParticipations")
      *       ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found",
      *          @OA\JsonContent()
      *      )
      * )
      */
    public function update(RoundParticipations $id, Request $request)
    {
      $this->validate($request, [
        'round_id' => ['nullable', 'integer'],
        'player_id' => ['nullable', 'integer'],
        'response_id' => ['nullable', 'integer'],
        'win' => 'nullable',
      ]);

      $roundParticipations = RoundParticipations::find($id);

      try {
        $request->round_id ? $roundParticipations->round_id = $request->round_id : false;
        $request->player_id ? $roundParticipations->player_id = $request->player_id : false;
        $request->response_id ? $roundParticipations->response_id = $request->response_id : false;
        $request->win ? $roundParticipations->win = $request->win : false;
        $roundParticipations->save();

        return response()->json($roundParticipations, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=> 'Round Players Not Found'], 404);
    }


     /**
      * @OA\Delete(
      *      path="/v1/auth/round-players/{id}",
      *      tags={"Round Players"},
      *      summary="Delete round players by id",
      *      description="Deletes a record and returns no content",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Round Players id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=204,
      *          description="Successful operation",
      *          @OA\JsonContent()
      *       ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden"
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found"
      *      )
      * )
      */
    public function destroy(RoundParticipations $id)
    {
        //
    }
}
