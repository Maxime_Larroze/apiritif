<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

  /**
   *  @OA\Post(
   *      path="/v1/public/login",
   *      summary="Login",
   *      tags={"Login"},
   *      description="Returns auth user and his token",
   *      @OA\RequestBody(
   *          required=true,
   *          @OA\JsonContent(
   *              @OA\Property(property="username", format="string", default="username"),
   *              @OA\Property(property="password", format="string", default="password"),
   *          )
   *      ),
   *      @OA\Response(
   *          response=200,
   *          description="Successful operation",
   *          @OA\JsonContent(ref="#/components/schemas/User")
   *      ),
   *      @OA\Response(
   *          response=401,
   *          description="Unauthenticated",
   *          @OA\MediaType(
   *            mediaType="application/json",
   *          )
   *      ),
   *      @OA\Response(
   *        response=400,
   *        description="Bad Request"
   *      ),
   *      @OA\Response(
   *        response=404,
   *        description="Not found"
   *      ),
   *  )
   */
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        try {
            if (Auth::attempt(['email' => $request->username, 'password' => $request->password], $request->remember)) {
                $token = $this->token();
                $user = User::find(Auth::user()->id);
                return response()->json(['user'=>$user, 'token'=>$this->getToken()], 200);
            }
        } catch (\Throwable $th) {
            return response()->json(['error'=>'Identifiant ou mot de passe incorrect'], 401);
        }
        return response()->json(['error'=>'Identifiant ou mot de passe incorrect'], 401);
    }

    public function logout()
    {
        $user = User::find(Auth::user()->id);
        $user->tokens()->delete();
        Auth::logout();
        return response()->json(['msg'=>'Vous êtes déconencté'], 200);
    }
}
