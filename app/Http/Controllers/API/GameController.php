<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Http\Requests\StoreGameRequest;
use App\Http\Requests\UpdateGameRequest;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     *  @OA\Get(
     *      path="/v1/auth/games",
     *      tags={"Game"},
     *      summary="Get list of Games",
     *      description="Returns all games",
     *      security={{ "apiAuth": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Game")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\MediaType(
     *            mediaType="application/json",
     *          )
     *      ),
     *      @OA\Response(
     *        response=400,
     *        description="Bad Request"
     *      ),
     *      @OA\Response(
     *        response=404,
     *        description="not found"
     *      ),
     *  )
     */
    public function index()
    {
        $games = Game::all();
        return response()->json($games, 200);
    }


    /**
     * @OA\Post(
     *      path="/v1/auth/games",
     *      tags={"Game"},
     *      summary="Store new game",
     *      description="Returns game data",
     *      security={{ "apiAuth": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="type", format="string", default="type", description="Values:[Quizz,JeuVote]"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Game")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => ['required', 'in:Quizz,JeuVote'],
        ]);

      try {
        $game = Game::create([
          'type' => $request->type,
        ]);
        return response()->json($game, 201);
      }
      catch (\Throwable $th) {
        return response()->json(['error' => 'Unauthorized'], 403);
      }
      return response()->json(['error' => 'Unauthorized'], 403);
    }


    /**
      *  @OA\Get(
      *      path="/v1/auth/games/{id}",
      *      tags={"Game"},
      *      summary="Get one Game",
      *      description="Returns one game",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Game id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Game")
      *      ),
      *      @OA\Response(
      *         response=400,
      *         description="Bad Request",
      *         @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *         response=401,
      *         description="Unauthenticated",
      *         @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *         response=403,
      *         description="Forbidden",
      *         @OA\JsonContent()
      *      )
      *  )
      */
    public function show(Game $id)
    {
        $game = Game::find($id);
      try {
        return response()->json($game, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error' => 'Unauthorized'], 403);
      }
      return response()->json(['error' => 'Game Not Found'], 404);
    }


    /**
      * @OA\Put(
      *      path="/v1/auth/games/{id}",
      *      tags={"Game"},
      *      summary="Update existing game",
      *      description="Returns updated game data",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Game id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\RequestBody(
      *          required=true,
      *          @OA\JsonContent(
      *              @OA\Property(property="type", format="string", default="type", description="Values:[Quizz,JeuVote]"),
      *          )
      *      ),
      *      @OA\Response(
      *          response=202,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Game")
      *       ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found",
      *          @OA\JsonContent()
      *      )
      * )
      */
    public function update(Game $id, Request $request)
    {
        $this->validate($request, [
          'type' => ['required', 'in:Quizz,JeuVote'],
        ]);

        $game = Game::find($id);

        try{
          $game->type = $request->type;
          $game->save();
          return response()->json($game, 200);
        }
        catch (\Throwable $th) {
          return response()->json(['error' => 'Unauthorized'], 403);
        }
        return response()->json(['error' => 'Game Not Found'], 404);
    }


    /**
     * @OA\Delete(
     *      path="/v1/auth/games/{id}",
     *      tags={"Game"},
     *      summary="Delete a game by id",
     *      description="Deletes a record and returns no content",
     *      security={{ "apiAuth": {} }},
     *      @OA\Parameter(
     *          name="id",
     *          description="Game id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(Game $id)
    {
        //Voir pour deletion en cascade avec le jeu attaché.
    }
}
