<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\InfosParticipant;
use App\Http\Requests\StoreInfosParticipantRequest;
use App\Http\Requests\UpdateInfosParticipantRequest;
use Illuminate\Http\Request;

class InfosParticipantController extends Controller
{

    /**
     *  @OA\Get(
     *      path="/v1/auth/players",
     *      tags={"Player"},
     *      summary="Get list of Players",
     *      description="Returns all players",
     *      security={{ "apiAuth": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/InfosParticipant")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\MediaType(
     *            mediaType="application/json",
     *          )
     *      ),
     *      @OA\Response(
     *        response=400,
     *        description="Bad Request"
     *      ),
     *      @OA\Response(
     *        response=404,
     *        description="Not found"
     *      ),
     *  )
     */
    public function index()
    {
        $infosParticipants = InfosParticipant::all();
        return response()->json($infosParticipants, 200);
    }


    /**
     * @OA\Post(
     *      path="/v1/auth/players",
     *      tags={"Player"},
     *      summary="Store new player",
     *      description="Returns player data",
     *      security={{ "apiAuth": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="party_id", format="integer", default="1"),
     *              @OA\Property(property="player_id", format="integer", default="1"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/InfosParticipant")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'party_id' => ['required', 'integer'],
          'player_id'  => ['required', 'integer'],
      ]);

      try {
        $infosParticipant = InfosParticipant::create([
          'party_id' => $request->party_id,
          'player_id'  => $request->player_id,
          // 'play_quizz' => $request->play_quizz ?? null,
          // 'play_action' => $request->play_action ?? null,
          // 'play_verite' => $request->play_verite ?? null,
          // 'play_mime' => $request->play_mime ?? null,
          // 'play_duel' => $request->play_duel ?? null,
          // 'points' => $request->points,
        ]);
        return response()->json($infosParticipant, 201);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Unauthorized'], 403);
    }


     /**
      *  @OA\Get(
      *      path="/v1/auth/players/{id}",
      *      tags={"Player"},
      *      summary="Get one Player",
      *      description="Returns one player",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Player id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/InfosParticipant")
      *      ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      )
      *  )
      */
    public function show(InfosParticipant $id)
    {
      $infosParticipant = InfosParticipant::find($id);
      try {
        return response()->json($infosParticipant, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Player Not Found'], 404);
    }


     /**
      * @OA\Put(
      *      path="/v1/auth/players/{id}",
      *      tags={"Player"},
      *      summary="Update existing player",
      *      description="Returns updated player data",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Player id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\RequestBody(
      *          required=true,
      *          @OA\JsonContent(
      *              @OA\Property(property="play_quizz", format="boolean", default="0", description="if play quizz > true"),
      *              @OA\Property(property="play_action", format="boolean", default="0", description="if play action > true"),
      *              @OA\Property(property="play_verite", format="boolean", default="0", description="if play verite > true"),
      *              @OA\Property(property="play_mime", format="boolean", default="0", description="if play mime > true"),
      *              @OA\Property(property="play_duel", format="boolean", default="0", description="if play duel > true"),
      *              @OA\Property(property="points", format="integer", default="10", description="number of points winned"),
      *          )
      *      ),
      *      @OA\Response(
      *          response=202,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/InfosParticipant")
      *       ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found",
      *          @OA\JsonContent()
      *      )
      * )
      */
    public function update(InfosParticipant $id, Request $request)
    {
      $this->validate($request, [
        'play_quizz' => 'nullable',
        'play_action' => 'nullable',
        'play_verite' => 'nullable',
        'play_mime' => 'nullable',
        'play_duel' => 'nullable',
        'points' => 'nullable',
      ]);

      $infosParticipant = InfosParticipant::find($id);
      
      try{
        $request->play_quizz ? $infosParticipant->play_quizz = $request->play_quizz : false;
        $request->play_action ? $infosParticipant->play_action = $request->play_action : false;
        $request->play_verite ? $infosParticipant->play_verite = $request->play_verite : false;
        $request->play_mime ? $infosParticipant->play_mime = $request->play_mime : false;
        $request->play_duel ? $infosParticipant->play_duel = $request->play_duel : false;
        $request->points ? $infosParticipant->points += $request->points : false;
        $infosParticipant->save();

        return response()->json($infosParticipant, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Player Not Found'], 404);
    }


     /**
      * @OA\Delete(
      *      path="/v1/auth/players/{id}",
      *      tags={"Player"},
      *      summary="Delete a player by id",
      *      description="Deletes a record and returns no content",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Player id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=204,
      *          description="Successful operation",
      *          @OA\JsonContent()
      *       ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden"
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found"
      *      )
      * )
      */
    public function destroy(InfosParticipant $id)
    {
        //
    }
}
