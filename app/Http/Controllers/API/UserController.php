<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
     /**
      *  @OA\Get(
      *      path="/v1/auth/users",
      *      tags={"User"},
      *      summary="Get list of Users",
      *      description="Returns all users",
      *      security={{ "apiAuth": {} }},
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/User")
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\MediaType(
      *            mediaType="application/json",
      *          )
      *      ),
      *      @OA\Response(
      *        response=400,
      *        description="Bad Request"
      *      ),
      *      @OA\Response(
      *        response=404,
      *        description="Not found"
      *      ),
      *  )
      */
    public function index()
    {
      $users = User::all();
      return response()->json($users, 200);
    }


    /**
     * @OA\Post(
     *      path="/v1/auth/users",
     *      tags={"User"},
     *      summary="Store new user",
     *      description="Returns user data",
     *      security={{ "apiAuth": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="pseudo", format="string", default="username"),
     *              @OA\Property(property="email", format="string", default="username@mail.com"),
     *              @OA\Property(property="password", format="string", default="passwod"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'pseudo' => ['required', 'unique:users'],
        'email' => ['required', 'unique:users', 'email:rfc,dns'],
        'password' => ['required'],
      ]);

      try {
        $user = User::create([
          'pseudo' => $request->pseudo,
          'email' => $request->email,
          'password' => Hash::make($request->password),
        ]);
        return response()->json($user, 201);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Unauthorized'], 403);
    }


     /**
      *  @OA\Get(
      *      path="/v1/auth/users/{id}",
      *      tags={"User"},
      *      summary="Get one User",
      *      description="Returns one user",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="User id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/User")
      *      ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      )
      *  )
      */
    public function show(User $id)
    {
        $user = User::find($id);
        try {
          return response()->json($user, 200);
        }
        catch (\Throwable $th) {
          return response()->json(['error'=>'Unauthorized'], 403);
        }
        return response()->json(['error'=>'User Not Found'], 404);
    }


     /**
      * @OA\Put(
      *      path="/v1/auth/users/{id}",
      *      tags={"User"},
      *      summary="Update existing user",
      *      description="Returns updated user data",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="User id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\RequestBody(
      *          required=true,
      *          @OA\JsonContent(
      *              @OA\Property(property="pseudo", format="string", default="username"),
      *              @OA\Property(property="email", format="string", default="username@mail.com"),
      *              @OA\Property(property="password", format="string", default="passwod"),
      *          )
      *      ),
      *      @OA\Response(
      *          response=202,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/User")
      *       ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found",
      *          @OA\JsonContent()
      *      )
      * )
      */
    public function update(User $id, Request $request)
    {
      $this->validate($request, [
        'pseudo' => ['nullable', 'unique:users'],
        'email' => ['nullable', 'unique:users', 'email:rfc,dns'],
        'password' => ['nullable'],
      ]);

      $user = User::find($id);

      try {
        $request->pseudo ? $user->pseudo = $request->pseudo : false;
        $request->email ? $user->email = $request->email : false;
        $request->password ? $user->password = $request->password : false;
        $user->save();

        return response()->json($user, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'User Not Found'], 404);
    }


     /**
      * @OA\Delete(
      *      path="/v1/auth/users/{id}",
      *      tags={"User"},
      *      summary="Delete a user by id",
      *      description="Deletes a record and returns no content",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="User id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=204,
      *          description="Successful operation",
      *          @OA\JsonContent()
      *       ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden"
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found"
      *      )
      * )
      */
    public function destroy(User $id)
    {
        $user = User::find($id);

        try {
          $user->delete();
          return response()->json(['success'=>'User deleted with success'], 200);
        }
        catch (\Throwable $th) {
          return response()->json(['error'=>'Unauthorized'], 403);
        }
        return response()->json(['error'=>'User Not Found'], 404);
    }
}
