<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Quizz;
use App\Http\Requests\StoreQuizzRequest;
use App\Http\Requests\UpdateQuizzRequest;
use Illuminate\Http\Request;

class QuizzController extends Controller
{

    /**
      *  @OA\Get(
      *      path="/v1/auth/quizzs",
      *      tags={"Quizz"},
      *      summary="Get list of Quizzs",
      *      description="Returns all quizzs",
      *      security={{ "apiAuth": {} }},
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Quizz")
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\MediaType(
      *            mediaType="application/json",
      *          )
      *      ),
      *      @OA\Response(
      *        response=400,
      *        description="Bad Request"
      *      ),
      *      @OA\Response(
      *        response=404,
      *        description="Not found"
      *      ),
      *  )
      */
    public function index()
    {
      $quizzs = Quizz::all();
      return response()->json($quizzs, 200);
    }


    /**
     * @OA\Post(
     *      path="/v1/auth/quizzs",
     *      tags={"Quizz"},
     *      summary="Store new quizz",
     *      description="Returns quizz data",
     *      security={{ "apiAuth": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="question", format="string", default="Lorem ipsum ?"),
     *              @OA\Property(property="answer_1", format="string", default="Lorem ipsum 1"),
     *              @OA\Property(property="answer_2", format="string", default="Lorem ipsum 2"),
     *              @OA\Property(property="answer_3", format="string", default="Lorem ipsum 3"),
     *              @OA\Property(property="answer_4", format="string", default="Lorem ipsum 4"),
     *              @OA\Property(property="good_answer", format="integer", default="1"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Quizz")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'question' => ['required', 'string'],
          'answer_1' => ['required', 'string'],
          'answer_2' => ['required', 'string'],
          'answer_3' => ['required', 'string'],
          'answer_4' => ['required', 'string'],
          'good_answer' => ['required', 'integer'],
        ]);

        try {
          $quizz = Quizz::create([
            'question' => $request->question,
            'answer_1' => $request->answer_1,
            'answer_2' => $request->answer_2,
            'answer_3' => $request->answer_3,
            'answer_4' => $request->answer_4,
            'good_answer' => $request->good_answer,
          ]);
          return response()->json($quizz, 201);
        }
        catch (\Throwable $th) {
          return response()->json(['error'=>'Unauthorized'], 403);
        }
        return response()->json(['error'=>'Unauthorized'], 403);
      }


    /**
      *  @OA\Get(
      *      path="/v1/auth/quizzs/{id}",
      *      tags={"Quizz"},
      *      summary="Get one Quizz",
      *      description="Returns one quizz",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Quizz id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Quizz")
      *      ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      )
      *  )
      */
    public function show(Quizz $id)
    {
        $quizz = Quizz::find($id);
        try {
            return response()->json($quizz, 200);
        }
        catch (\Throwable $th) {
            return response()->json(['error'=>'Unauthorized'], 403);
        }
        return response()->json(['error'=>'Quizz Not Found'], 404);
    }


    /**
      * @OA\Put(
      *      path="/v1/auth/quizzs/{id}",
      *      tags={"Quizz"},
      *      summary="Update existing quizz",
      *      description="Returns updated quizz data",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Quizz id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\RequestBody(
      *          @OA\JsonContent(
      *              @OA\Property(property="question", format="string", default="Lorem ipsum ?"),
      *              @OA\Property(property="answer_1", format="string", default="Lorem ipsum 1"),
      *              @OA\Property(property="answer_2", format="string", default="Lorem ipsum 2"),
      *              @OA\Property(property="answer_3", format="string", default="Lorem ipsum 3"),
      *              @OA\Property(property="answer_4", format="string", default="Lorem ipsum 4"),
      *              @OA\Property(property="good_answer", format="integer", default="1"),
      *          )
      *      ),
      *      @OA\Response(
      *          response=202,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Quizz")
      *       ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found",
      *          @OA\JsonContent()
      *      )
      * )
      */
    public function update(Quizz $id, Request $request)
    {
      $this->validate($request, [
        'question' => ['required', 'string'],
        'answer_1' => ['required', 'string'],
        'answer_2' => ['required', 'string'],
        'answer_3' => ['required', 'string'],
        'answer_4' => ['required', 'string'],
        'good_answer' => ['required', 'integer'],
      ]);

      $quizz = Quizz::find($id);

      try {
        $request->question ? $quizz->question = $request->question : false;
        $request->answer_1 ? $quizz->answer_1 = $request->answer_1 : false;
        $request->answer_2 ? $quizz->answer_2 = $request->answer_2 : false;
        $request->answer_3 ? $quizz->answer_3 = $request->answer_3 : false;
        $request->answer_4 ? $quizz->answer_4 = $request->answer_4 : false;
        $request->good_answer ? $quizz->good_answer = $request->good_answer : false;
        $quizz->save();

        return response()->json($quizz, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Quizz Not Found'], 404);
    }


    /**
      * @OA\Delete(
      *      path="/v1/auth/quizzs/{id}",
      *      tags={"Quizz"},
      *      summary="Delete a quizz by id",
      *      description="Deletes a record and returns no content",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Quizz id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=204,
      *          description="Successful operation",
      *          @OA\JsonContent()
      *       ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden"
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found"
      *      )
      * )
      */
    public function destroy(Quizz $id)
    {
      $quizz = Quizz::find($id);

      try {
        $quizz->delete();
        return response()->json(['success'=>'Quizz deleted with success'], 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Quizz Not Found'], 404);
    }
}
