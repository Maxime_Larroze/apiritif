<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Round;
use App\Http\Requests\StoreRoundRequest;
use App\Http\Requests\UpdateRoundRequest;
use Illuminate\Http\Request;

class RoundController extends Controller
{
     /**
      *  @OA\Get(
      *      path="/v1/auth/rounds",
      *      tags={"Round"},
      *      summary="Get list of Rounds",
      *      description="Returns all rounds",
      *      security={{ "apiAuth": {} }},
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Round")
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\MediaType(
      *            mediaType="application/json",
      *          )
      *      ),
      *      @OA\Response(
      *        response=400,
      *        description="Bad Request"
      *      ),
      *      @OA\Response(
      *        response=404,
      *        description="Not found"
      *      ),
      *  )
      */
    public function index()
    {
      $rounds = Round::all();
      return response()->json($rounds, 200);
    }


    /**
     * @OA\Post(
     *      path="/v1/auth/rounds",
     *      tags={"Round"},
     *      summary="Store new round",
     *      description="Returns round data",
     *      security={{ "apiAuth": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="party_id", format="integer", default="1"),
     *              @OA\Property(property="game_id", format="integer", default="1"),
     *              @OA\Property(property="main_player_id", format="integer", default="1"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Round")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'party_id' => ['required', 'integer'],
        'game_id' => ['required', 'integer'],
        'main_player_id' => ['required', 'integer'],
      ]);

      try {
        $round = Round::create([
          'party_id' => $request->party_id,
          'game_id' => $request->game_id,
          'main_player_id' => $request->main_player_id,
        ]);
        return response()->json($round, 201);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Unauthorized'], 403);
    }


     /**
      *  @OA\Get(
      *      path="/v1/auth/rounds/{id}",
      *      tags={"Round"},
      *      summary="Get one Round",
      *      description="Returns one round",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Round id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Round")
      *      ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      )
      *  )
      */
    public function show(Round $id)
    {
        $round = Round::find($id);
        try {
            return response()->json($round, 200);
        }
        catch (\Throwable $th) {
            return response()->json(['error'=>'Unauthorized'], 403);
        }
        return response()->json(['error'=>'Round Not Found'], 404);
    }


     /**
      * @OA\Put(
      *      path="/v1/auth/rounds/{id}",
      *      tags={"Round"},
      *      summary="Update existing round",
      *      description="Returns updated round data",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Round id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\RequestBody(
      *          required=true,
      *          @OA\JsonContent(
      *              @OA\Property(property="party_id", format="integer", default=""),
      *              @OA\Property(property="game_id", format="integer", default=""),
      *              @OA\Property(property="main_player_id", format="integer", default=""),
      *          )
      *      ),
      *      @OA\Response(
      *          response=202,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Round")
      *       ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found",
      *          @OA\JsonContent()
      *      )
      * )
      */
    public function update(Round $id, Request $request)
    {
        $this->validate($request, [
          'party_id' => ['nullable', 'integer'],
          'game_id' => ['nullable', 'integer'],
          'main_player_id' => ['nullable', 'integer'],
        ]);

        $round = Round::find($id);

        try {
            $request->party_id ? $round->party_id = $request->party_id : false;
            $request->game_id ? $round->game_id = $request->game_id : false;
            $request->main_player_id ? $round->main_player_id = $request->main_player_id : false;
            $round->save();

            return response()->json($round, 200);
        }
        catch (\Throwable $th) {
            return response()->json(['error'=>'Unauthorized'], 403);
        }
        return response()->json(['error'=>'Round Not Found'], 404);
    }

    /**
      * @OA\Delete(
      *      path="/v1/auth/rounds/{id}",
      *      tags={"Round"},
      *      summary="Delete a round by id",
      *      description="Deletes a record and returns no content",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Round id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=204,
      *          description="Successful operation",
      *          @OA\JsonContent()
      *       ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden"
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found"
      *      )
      * )
      */
    public function destroy(Round $id)
    {
        //
    }
}
