<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\TypeGamePoints;
use App\Http\Requests\StoreTypeGamePointsRequest;
use App\Http\Requests\UpdateTypeGamePointsRequest;
use Illuminate\Http\Request;

class TypeGamePointsController extends Controller
{
    /**
      *  @OA\Get(
      *      path="/v1/auth/types",
      *      tags={"Type"},
      *      summary="Get list of Types",
      *      description="Returns all types",
      *      security={{ "apiAuth": {} }},
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/TypeGamePoints")
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\MediaType(
      *            mediaType="application/json",
      *          )
      *      ),
      *      @OA\Response(
      *        response=400,
      *        description="Bad Request"
      *      ),
      *      @OA\Response(
      *        response=404,
      *        description="Not found"
      *      ),
      *  )
      */
    public function index()
    {
      $typesGamePoints = TypeGamePoints::all();
      return response()->json($typesGamePoints, 200);
    }


    /**
     * @OA\Post(
     *      path="/v1/auth/types",
     *      tags={"Type"},
     *      summary="Store new type",
     *      description="Returns type data",
     *      security={{ "apiAuth": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="game_type", format="integer", default="Action"),
     *              @OA\Property(property="points", format="integer", default="10"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TypeGamePoints")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'game_type' => ['required', 'in:Action,Verité,Mime,Duel,Quizz'],
        'points' => ['required', 'integer'],
      ]);

      try {
        $typeGamePoints = TypeGamePoints::create([
          'game_type' => $request->game_type,
          'points' => $request->points,
        ]);
        return response()->json($typeGamePoints, 201);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Unauthorized'], 403);
    }


     /**
      *  @OA\Get(
      *      path="/v1/auth/types/{id}",
      *      tags={"Type"},
      *      summary="Get one Type",
      *      description="Returns one type",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Type id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/TypeGamePoints")
      *      ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      )
      *  )
      */
    public function show(TypeGamePoints $id)
    {
        $typeGamePoint = TypeGamePoints::find($id);

      try {
        return response()->json($typeGamePoint, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Type Note Found'], 404);
    }


     /**
      * @OA\Put(
      *      path="/v1/auth/types/{id}",
      *      tags={"Type"},
      *      summary="Update existing type",
      *      description="Returns updated type data",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Type id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\RequestBody(
      *          required=true,
      *          @OA\JsonContent(
      *              @OA\Property(property="game_type", format="integer", default=""),
      *              @OA\Property(property="points", format="integer", default=""),
      *          )
      *      ),
      *      @OA\Response(
      *          response=202,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/TypeGamePoints")
      *       ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found",
      *          @OA\JsonContent()
      *      )
      * )
      */
    public function update(TypeGamePoints $id, Request $request)
    {
      $this->validate($request, [
        'game_type' => ['nullable', 'in:Action,Verité,Mime,Duel,Quizz'],
        'points' => ['nullable', 'integer'],
      ]);

      $typeGamePoints = TypeGamePoints::find($id);

      try{
        $request->game_type ? $typeGamePoints->game_type = $request->game_type : false;
        $request->points ? $typeGamePoints->points = $request->points : false;
        $typeGamePoints->save();

        return response()->json($typeGamePoints, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Type Note Found'], 404);
    }


     /**
      * @OA\Delete(
      *      path="/v1/auth/types/{id}",
      *      tags={"Type"},
      *      summary="Delete a type by id",
      *      description="Deletes a record and returns no content",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Type id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=204,
      *          description="Successful operation",
      *          @OA\JsonContent()
      *       ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden"
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found"
      *      )
      * )
      */
    public function destroy(TypeGamePoints $id)
    {
        //
    }
}
