<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Party;
use App\Http\Requests\StorePartyRequest;
use App\Http\Requests\UpdatePartyRequest;
use Illuminate\Http\Request;

class PartyController extends Controller
{
     /**
      *  @OA\Get(
      *      path="/v1/auth/parties",
      *      tags={"Party"},
      *      summary="Get list of Parties",
      *      description="Returns all parties",
      *      security={{ "apiAuth": {} }},
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Party")
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\MediaType(
      *            mediaType="application/json",
      *          )
      *      ),
      *      @OA\Response(
      *        response=400,
      *        description="Bad Request"
      *      ),
      *      @OA\Response(
      *        response=404,
      *        description="Not found"
      *      ),
      *  )
      */
    public function index()
    {
        $parties = Party::all();
        return response()->json($parties, 200);
    }


    /**
     * @OA\Post(
     *      path="/v1/auth/parties",
     *      tags={"Party"},
     *      summary="Store new party",
     *      description="Returns party data",
     *      security={{ "apiAuth": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="choice", format="string", default="string", description="which type of game you want to play"),
     *              @OA\Property(property="nb_turns", format="integer", default="5", description="number of turns"),
     *              @OA\Property(property="admin_player", format="string", default="", description="admin player id"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Party")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'choice' => ['required', 'string'],
        'nb_turns' => ['required', 'integer'],
        'admin_player' => ['nullable', 'integer'],
      ]);

      try {
        $party = Party::create([
          'choice' => $request->choice,
          'nb_turns' => $request->nb_turns,
          'admin_player' => $request->admin_player ?? null,
        ]);
        return response()->json($party, 201);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Unauthorized'], 403);
    }


     /**
      *  @OA\Get(
      *      path="/v1/auth/parties/{id}",
      *      tags={"Party"},
      *      summary="Get one Party",
      *      description="Returns one party",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Party id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=200,
      *          description="Successful operation",
      *          @OA\JsonContent(ref="#/components/schemas/Party")
      *      ),
      *      @OA\Response(
      *          response=400,
      *          description="Bad Request",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden",
      *          @OA\JsonContent()
      *      )
      *  )
      */
    public function show(Party $id)
    {
        $party = Party::find($id);
        try {
            return response()->json($party, 200);
        }
        catch (\Throwable $th) {
            return response()->json(['error'=>'Unauthorized'], 403);
        }
        return response()->json(['error'=>'Party Not Found'], 404);
    }


    /**
     * @OA\Put(
     *      path="/v1/auth/parties/{id}",
     *      tags={"Party"},
     *      summary="Update existing party",
     *      description="Returns updated party data",
     *      security={{ "apiAuth": {} }},
     *      @OA\Parameter(
     *          name="id",
     *          description="Party id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="choice", format="string", default="string", description="which type of game you want to play"),
     *              @OA\Property(property="nb_turns", format="integer", default="5", description="number of turns"),
     *              @OA\Property(property="admin_player", format="string", default="", description="admin player id"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Party")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function update(Party $id, Request $request)
    {
      $this->validate($request, [
        'choice' => ['nullable', 'string'],
        'nb_turns' => ['nullable', 'integer'],
        'admin_player' => ['nullable', 'integer'],
      ]);

      $party = Party::find($id);
      
      try{
        $request->choice ? $party->choice = $request->choice : false;
        $request->nb_turns ? $party->nb_turns = $request->nb_turns : false;
        $request->admin_player ? $party->admin_player = $request->admin_player : false;
        $party->save();

        return response()->json($party, 200);
      }
      catch (\Throwable $th) {
        return response()->json(['error'=>'Unauthorized'], 403);
      }
      return response()->json(['error'=>'Party Not Found'], 404);
    }


     /**
      * @OA\Delete(
      *      path="/v1/auth/parties/{id}",
      *      tags={"Party"},
      *      summary="Delete a party by id",
      *      description="Deletes a record and returns no content",
      *      security={{ "apiAuth": {} }},
      *      @OA\Parameter(
      *          name="id",
      *          description="Party id",
      *          required=true,
      *          in="path",
      *          @OA\Schema(
      *              type="integer"
      *          )
      *      ),
      *      @OA\Response(
      *          response=204,
      *          description="Successful operation",
      *          @OA\JsonContent()
      *       ),
      *      @OA\Response(
      *          response=401,
      *          description="Unauthenticated",
      *          @OA\JsonContent()
      *      ),
      *      @OA\Response(
      *          response=403,
      *          description="Forbidden"
      *      ),
      *      @OA\Response(
      *          response=404,
      *          description="Resource Not Found"
      *      )
      * )
      */
    public function destroy(Party $id)
    {
        //
    }
}
