<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Api'ritif Documentation",
 *      description="Implementation of Swagger with in Laravel",
 *      @OA\Contact(
 *          email="admin@admin.com"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Demo API Server"
 * )
 *
 * @OA\SecurityScheme(
 *     type="http",
 *     description="Login with email and password to get the authentication token",
 *     name="Token based Based",
 *     in="header",
 *     scheme="bearer",
 *     bearerFormat="JWT",
 *     securityScheme="apiAuth",
 * )
 *
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $uri = "http://apiritif.test/api/v1/";
    public function setURI($URI){$this->uri = $URI;}
    public function getURI(){return $this->uri;}

    protected $token;
    public function setToken($token){$this->token = $token;}
    public function getToken(){return $this->token;}

    public function http()
    {
        return Http::accept('application/json')->withHeaders(['Authorization' => 'Bearer '.$this->getToken()]);
    }

    public function statusCode($response)
    {
        if($response->status() >= 300 && $response->status()<400){return back()->withErrors(['error'=>'Code HTTP 300x']);}
        if($response->status() >= 400 && $response->status()<500){return back()->withErrors(['error'=>'Erreur du client HTTP']);}
        if($response->status() >= 500 && $response->status()<600){return back()->withErrors(['error'=>'Erreur du serveur interne']);}
    }

    public function token()
    {
        $token = User::find(Auth::user()->id)->CSToken();
        $this->setToken($token);
        return $this->getToken();
    }


    // public $http = Http::header(['Authorization' => 'Bearer '.$token]);

}
