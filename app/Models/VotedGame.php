<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="VotedGame",
 *     description="VotedGame model",
 *     @OA\Xml(
 *         name="VotedGame"
 *     )
 * )
 */
class VotedGame extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'voted_games';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    // /
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array<string>
    //  */
    protected $fillable = [
        'id',
        'instruction',
        'type',
        'answer_1',
        'answer_2',
        'created_at',
        'updated_at'
    ];


}
