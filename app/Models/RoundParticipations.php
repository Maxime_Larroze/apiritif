<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="RoundParticipations",
 *     description="RoundParticipations model",
 *     @OA\Xml(
 *         name="RoundParticipations"
 *     )
 * )
 */
class RoundParticipations extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'round_participations';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'id',
        'round_id',
        'player_id',
        'response_id',
        'win',
        'created_at',
        'updated_at'
    ];

    // Relations ...
   public function round()
   {
       return $this->hasOne(Round::class, 'round_id', 'id');
   }

}
