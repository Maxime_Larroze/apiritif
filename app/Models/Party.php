<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Party",
 *     description="Party model",
 *     @OA\Xml(
 *         name="Party"
 *     )
 * )
 */
class Party extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'parties';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    // /
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array<string>
    //  */
    protected $fillable = [
      'id',
      'choice',
      'nb_turns',
      'admin_player',
      'created_at',
      'updated_at'
    ];

    // Relations ...
   public function infos_participants()
   {
       return $this->hasMany(InfosParticipant::class, 'id', 'party_id');
   }

   public function rounds()
   {
       return $this->hasMany(Round::class, 'id', 'party_id');
   }
}
