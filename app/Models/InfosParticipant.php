<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="InfosParticipant",
 *     description="InfosParticipant model",
 *     @OA\Xml(
 *         name="InfosParticipant"
 *     )
 * )
 */
class InfosParticipant extends Model
{
    use HasFactory;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'infos_participants';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    // /
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array<string>
    //  */
    protected $fillable = [
        'id',
        'party_id',
        'player_id',
        'play_quizz',
        'play_action',
        'play_verite',
        'play_mime',
        'play_duel',
        'points',
        'created_at',
        'updated_at'
    ];

}
