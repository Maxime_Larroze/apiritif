<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Quizz",
 *     description="Quizz model",
 *     @OA\Xml(
 *         name="Quizz"
 *     )
 * )
 */
class Quizz extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'quizzs';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    // /
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array<int, string>
    //  */
    protected $fillable = [
        'id',
        'question',
        'answer_1',
        'answer_2',
        'answer_3',
        'answer_4',
        'good_answer',
        'created_at',
        'updated_at'
    ];


}
