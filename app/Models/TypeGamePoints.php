<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="TypeGamePoints",
 *     description="TypeGamePoints model",
 *     @OA\Xml(
 *         name="TypeGamePoints"
 *     )
 * )
 */
class TypeGamePoints extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'type_game_points';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    // /
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array<string>
    //  */
    protected $fillable = [
        'id',
        'game_type',
        'points',
        'created_at',
        'updated_at'
    ];

}
