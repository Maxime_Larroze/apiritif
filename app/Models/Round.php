<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Round",
 *     description="Round model",
 *     @OA\Xml(
 *         name="Round"
 *     )
 * )
 */
class Round extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rounds';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    // /
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array<string>
    //  */
    protected $fillable = [
        'id',
        'party_id',
        'game_id',
        'main_player_id',
        'created_at',
        'updated_at'
    ];

    // Relations ...
   public function round_participations()
   {
       return $this->hasMany(RoundParticipations::class, 'id', 'round_id');
   }

   public function party()
   {
       return $this->hasOne(Party::class, 'party_id', 'id');
   }
}
