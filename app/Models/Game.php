<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Game",
 *     description="Game model",
 *     @OA\Xml(
 *         name="Game"
 *     )
 * )
 */
class Game extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'games';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'id',
        'type',
        'created_at',
        'updated_at'
    ];

    // Relations ...
   public function round_participations()
   {
       return $this->hasMany(RoundParticipations::class, 'id', 'round_id');
   }

 }
